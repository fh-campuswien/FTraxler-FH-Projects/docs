# Protocol Appendix

## Wireshark Analysis of DHCP

## Wireshark Analysis of DNS (bind9)

## Wireshark Analysis of POSTFIX

## Description of DOVECOT

**Dovecot**  is an open-source IMAP  and  POP3  server for Linux/UNIX-like systems, written primarily with security in mind ("Secure by design"). Timo Sirainen originated Dovecot and first released it in July 2002. Dovecot developers primarily aim to produce a lightweight, fast and easy-to-set-up open-source  mailserver ("Mailserver").  
  
The primary purpose of dovecot is to act as mail storage server. Mail is delivered to the server using some  MDA ("Mail delivery agent")  and stored for later access with  MUA ("Mail user agent"). Dovecot can also act as mail  proxy ("Proxy server")  server, forwarding connection to another mail server, or act as a lightweight MUA in order to retrieve and manipulate mail on remote server for e.g. mail migration.


## Description of IPTABLES / UFW

Iptables is a generic table structure that defines rules and commands as part of the netfilter framework that facilitates Network Address Translation (NAT), packet filtering, and packet mangling in the Linux 2.4 and later operating systems. NAT is the process of converting an Internet Protocol address (IP address) into another IP address. Packet filtering is the process of passing or blocking packets at a network interface based on source and destination addresses, ports, or protocols. Packet mangling is the ability to alter or modify packets before and/or after routing.

##  NAT Types
### Static NAT (one to one mapping)
private IP is mapped to a static public IP 
```
iptables -t nat -A POSTROUTING -s {inside_local} -j SNAT --to {inside_global}
iptables -t nat -A PREROUTING -d {inside_global} -j DNAT --to {inside_local}
```
### Dynamic NAT (pool to pool mapping)
private IPis dynamically mapped to a public IP from an IP pool. (May differ over time)
```
iptables -t nat -A POSTROUTING -s {inside_local_pool} -j SNAT --to {inside_global_pool}
iptables -t nat -A PREROUTING -d {inside_global} -j DNAT --to {inside_local_pool}
```
### Dynamic NAT with PAT overloading
private IP is not always  the same `publicIP:port` combination
- Implement with `MASQUERADE` when you have a dynamic public IP 
`iptables -t nat -A POSTROUTING -o ens33 -j MASQUERADE`
- Implement with `SNAT` when you have a static public IP
`iptables -t nat -A POSTROUTING -s {inside_local}/pefixlength -j SNAT --to {inside_global}`

### Port Forwarding
`iptables -t nat -A PREROUTING -d {inside_global} –p {layer4_protocol} --dport {destination_port} -j DNAT --to-destination {inside_local:inside_local_port}`

## Other Questions:

### What is the difference between MUA, MTA and E-Mail Client?
**MUA**  (Mail User Agent) = Software the client uses to send/receive mails
**MTA** (Mail Transport Agent) = Mail server communicating via SMTP protocol. 
**MDA** (Mail Delivery Agent) = Inbox server which waites for the MUA to access the deliverd mails.
<!--stackedit_data:
eyJoaXN0b3J5IjpbNTQ3MjQ3OTk3LC01Mjg3MjI0OTQsOTA3NT
U3NF19
-->