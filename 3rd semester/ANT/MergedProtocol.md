# Ant Lab Merged Protocols 1-4
by Florian Traxler

# Lab 1

## System:
-   VMware 
-   Ubuntuserver 18.x

## Network Plan
-   **TODO** create a network plan 
- Host-IP: `192.168.10.135`

## Basic setup

### Enable SSH
(is enabled per default but not for root login)

### Check the Network
-  use `nmap` for host discovery
-  use `tcpdump`, `netstat` and `wireshark`
-  access their webpages with `curl` and e.g. `lynx` as browser

### Disable sending echo requests
-   `echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all`


# Lab 2

## Configure Gateway

### Configure Route
- Add route with `ip route add <internal_IP>/<sn> dev <eth0>`.
- To enable IP forward permanent add the line below to `/etc/sysctl.conf`:
`net.ipv4.ip_forward = 1`
- Set your own IP in `/etc/netplan/<strange file name>` (Yes, thats Ubuntu. Nothing really works as usual)
- To apply changes run `netplan apply`
- setup snat when using static IPs
`iptables -t nat -A POSTROUTING ! -d <internal_IP>/<sn> -o eth1 -j SNAT --to-source <external_IP>`
- otherwise use masquerade
`iptables -t nat -A POSTROUTING ! -d <internal_IP>/<sn> -o eth1 -j MASQUERADE`

### Configure Client
- `ip  route  add  default  via <gateway_IP>  dev  <eth0>`

### Troubleshooting
- `route -n`
- `route del default`
- `ip route list`
- `ip link`

## Setup DHCP
*Package:* `isc-dhcp-server`

### Configure DHCP
*Interface Config File:* `/etc/default/isc-dhcp-server`  
Set the interface you want to use for DHCP services.
  
*Config File:* `/etc/dhcp/dhcpd.conf`
```
option domain-name "<domain>";  
option domain-name-servers ns1.<domain>, ns2.<domain>;  
default-lease-time 3600;  
max-lease-time 7200;  
authoritative;

subnet <net> netmask <mask> {  
option routers <gateway_IP>;  
option subnet-mask <mask>;  
option domain-search "<domain>";  
option domain-name-servers <dns1_IP>, <dns2_IP>; 
option broadcast-address <broadcast_IP>; 
range <lower> <upper>;  
}
```

#### Static IPs
```
host <node> {  
hardware ethernet <mac>;  
fixed-address <node_IP>;  
}
```

### Enable On reboot
```
systemctl start isc-dhcp-server  
systemctl enable isc-dhcp-server
```

### Persist iptables
```
add-apt-repository universe
apt-get install iptables-persistent
```

### Enable on Clients:
(see above `netplan`)

*File:* `/etc/network/interfaces`
```
auto <eth0>  
iface <eth0> inet dhcp
```

### Restart Services
```
service isc-dhcp-server restart
netplan apply
```

## Setup DNS
*Package:* `bind9`

### Configure Bind
*Config File:* `/etc/bind/named.conf.options`

```
acl "clients" {
        127.0.0.0/8;
        <network>/<sn>;
};

options {
        directory "/var/cache/bind";
        recursion yes;
        allow-recursion { clients; };
        allow-query { clients; };
        allow-transfer { localhost; <ns2-addr>; }; secondary dns
        listen-on { localhost; <ns2-addr>; }; primary dns
        forwarders {
                1.1.1.1; upper level primary dns
                8.8.4.4; upper level secondary dns
        };
};
```

### Resolve own Server domain
*Config File:* `/etc/bind/named.conf.local`

```
zone "<domain>" {
type master;
file "/etc/bind/zones/<domain>.db";
};
 
zone "X.X.X.in-addr.arpa" {
type master;
file "/etc/bind/zones/rev.X.X.X.in-addr.arpa";
};
```

#### Secondary DNS

```
zone "<domain>" {
type slave;
file "/etc/bind/zones/<domain>.db";
masters {
<ns1.name>;
};
};
 
zone "X.X.X.in-addr.arpa" {
type slave;
file "/etc/bind/zones/rev.X.X.X.in-addr.arpa";
masters {
<ns1.name>;
};
};
```

#### Configure host lookup
In `/etc/bind` create folder `zones`.
Create file `<domain>.db` with content:
```
; BIND data file for <domain>
;
$ORIGIN <domain>
$TTL 14400
@ IN SOA ns1.<domain>. host.<domain>. (
201006601 ; Serial
7200 ; Refresh
120 ; Retry
2419200 ; Expire
604800) ; Default TTL
;
<domain>. IN NS ns1.<domain>.
<domain>. IN NS ns2.<domain>.

; mail configuration 
<domain>. IN MX 10 mail.<domain>.
mail IN A <mail_IP>

; root domain
<domain>. IN A <IP>

; nameserver
ns1 IN A <ns1_IP>
ns2 IN A <ns2_IP>

; www subdomain
www IN CNAME <domain.com>.

; ftp subdomain
// ftp IN CNAME <domain>.
```

#### Configure reverse DNS lookup
 Add file `rev.X.X.X.in-addr.arpa` with contents:
 ```
$ORIGIN rev.X.X.X.in-addr.arpa.
@ IN SOA <domain>. host.<domain>. (
2010081401;
28800;
604800;
604800;
86400 );
 
@ IN NS ns1.<domain>.
@ IN NS ns2.<domain>.
<host-addr> IN PTR <domain>.
```

#### Set resolve
In file `/etc/resolv.conf` add `search <domain>` to the top.

#### Restart service
`/etc/init.d/bind9 restart`

### Check for Connectivity
- `nslookup`
- `ping`


# Lab 3

## SetUp Email Server

### Install routine
- Install `apache2`
- Install `php`
- Install `postfix` (install will run a setup. Configure it for `internet site`)
- Restart `postfix`
- Install `dovecot-imapd` (and `dovecot-pop3d` if you'd like to use pop3)
- Restart `dovecot`

### Check with Sendmail
```
sendmail target@domain
To: target@domain // optional
From: sender@ domain // optional
Subject: Hello World 
Content
// blank space or your content -> issue Key Command "Ctrl + D" to exit
```
Your messages can be read with the command  `cat /var/mail/<user>`

## Manage EMail Server

### Add Users
Add new users as users on the machine and set a password with `useradd <myusername>`  
Be sure, the folderns in `home` directory do exist!

## Troubleshooting
LogFile: `/var/log/mail.err`
Reconfigure: `dpkg-reconfigure <package>`


# Lab 4

## Setup a Firewall
- Show iptables content `iptables -L`
- Shell script with rules is in the appendix

## Set up an FTP server
*Package:* `vsftpd`  
*Config file:* `/etc/vsftpd.conf`  

### Firewall rules
Are set in the Firewall script

### Create a new ftp user and directories
Just add a new user to the system with `adduser`

### Config
```
# Allow anonymous FTP? (Disabled by default).
anonymous_enable=NO
#
# Uncomment this to allow local users to log in.
local_enable=YES
write_enable=YES
chroot_local_user=YES
pasv_min_port=40000
pasv_max_port=50000
```
We’ll add a `user_sub_token` in order to insert the username in our `local_root directory` path so our configuration will work for this user and any future users that might be added.
```
user_sub_token=$USER
local_root=/home/$USER/ftp
```
Since we’re only planning to allow FTP access on a case-by-case basis, we’ll set up the configuration so that access is given to a user only when they are explicitly added to a list rather than by default:
```
userlist_enable=YES
userlist_file=/etc/vsftpd.userlist
userlist_deny=NO
```
Restart `vsftpd`  
Test the connection.

### Secure the connection

#### Create an SSL certificate
`openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/vsftpd.pem -out /etc/ssl/private/vsftpd.pem`

#### Change the Config
```
rsa_cert_file=/etc/ssl/private/vsftpd.pem
rsa_private_key_file=/etc/ssl/private/vsftpd.pem
ssl_enable=YES
```
Disable anonymous acces and only allow secure access (using TLS)
```
allow_anon_ssl=NO
force_local_data_ssl=YES
force_local_logins_ssl=YES
ssl_tlsv1=YES
ssl_sslv2=NO
ssl_sslv3=NO
require_ssl_reuse=NO
ssl_ciphers=HIGH
```
Restart `vsftpd`  
Test the connection with `sftp <user>@<server>`

## Monitor the devices
[Source][https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-an-snmp-daemon-and-client-on-ubuntu-14-04]

### Installation
*Manager:* `snmp snmp-mibs-downloader`
*Client:* `snmpd`
*Config:* `/etc/snmp/snmp.conf`

### Config
**TODO** 


# APPENDIX

## Command List
```
tcpdump -i <eth>
tcpdump src port <port>
tcpdump dst port <port>
tcpdump not icmp // hide icmp traffic
tcpdump arp // display arp traffic
nmap -sP <addr>/<sn>
netstat -a //all sockets
netstat -c //print every second
netstat -l // only listening
netstat -at/au // tcp/udp connections
netstat -r // routing table
netstat -i // all interfaces and connections
netstat -p // PID and name of corresponding program
netstat -s // summary for each protocol
telnet <addr> <port>
curl
nslookup
ping
iptables -L
iptstate
ip link set <interface> up/down
route add default gw <ip>
route add -net <nw>/<sn> gw <gateway_IP> metric <value> <interface>
route del <nw>
```

## Install and Config Scripts

### Gateway Setup
```
#!/bin/sh
#===========================================================================#
# add apt 'universe' repository
add-apt-repository universe
apt-get update
#===========================================================================#
# install services
apt-get install -y iptables-persistent
apt-get install openssh-server
ssh-keygen
apt-get install bind9
# troubleshooting toolset
apt-get install nmap
apt-get install tcpdump
apt-get install iptstate
#===========================================================================#
echo "DONE"
#===========================================================================#
```

### DHCP Setup and Config
```
#!/bin/sh
#===========================================================================#
#========================= CONFIG ==========================#
MAILSERVER_MAC=00:0c:29:4c:5b:de
MAILSERVER_IP=10.0.5.40
WEBSERVER_MAC=00:0c:29:3e:02:07
WEBSERVER_IP=10.0.5.80
MYHOSTNAME=ftraxler.ant.nwlab
#===========================================================================#
# Install services
apt-get install isc-dhcp-server
#===========================================================================#
> /etc/dhcp/dhcpd.conf
cat > /etc/dhcp/dhcpd.conf << EOL

option domain-name "$MYHOSTNAME";
option domain-name-servers ns1.$MYHOSTNAME, ns2.$MYHOSTNAME;
default-lease-time 3600;
max-lease-time 7200;
authoritative;

subnet 10.0.5.0 netmask 255.255.255.0 {
        option routers 10.0.5.1;
        option subnet-mask 255.255.255.0;
        option domain-search "$MYHOSTNAME";
        option broadcast-address 10.0.5.255;
        option domain-name-servers 10.0.5.1, 10.0.5.2;
        range 10.0.5.150 10.0.5.250;
}

host webserver {
        hardware ethernet $WEBSERVER_MAC;
        fixed-address $WEBSERVER_IP;
}

host mailserver {
        hardware ethernet $MAILSERVER_MAC;
        fixed-address $MAILSERVER_IP;
}

EOL
#===========================================================================#
echo "DONE"
#===========================================================================#
```

### DNS Setup and Config
```
#!/bin/sh
#===========================================================================#
#========================= CONFIG ==========================#
MAILSERVER_IP=10.0.5.40
WEBSERVER_IP=10.0.5.80
MYHOSTNAME=ftraxler.ant.nwlab
#===========================================================================#
# Install services
apt-get install bind9
# Setup file structure
mkdir /etc/bind/zones
#===========================================================================#
#=================== named.conf.options ====================#
> /etc/bind/named.conf.options
cat > /etc/bind/named.conf.options << EOL

acl "clients" {
        127.0.0.0/8;
        10.0.5.0/24;
};

options {
        directory "/var/cache/bind";
        recursion yes;
        allow-recursion { clients; };
        allow-query { clients; };
        allow-transfer { localhost; 10.0.5.2; };
        listen-on { localhost; 10.0.5.1; };
        forwarders {
                1.1.1.1;
                8.8.4.4;
        };
};
EOL
#=================== named.conf.local ====================#
> /etc/bind/named.conf.local
cat > /etc/bind/named.conf.local << EOL
zone "$MYHOSTNAME" {
type master;
file "/etc/bind/zones/$MYHOSTNAME.db";
};

zone "5.0.10.in-addr.arpa" {
type master;
file "/etc/bind/zones/rev.5.0.10.in-addr.arpa";
};
#=================== dns lookup ====================#
cat > /etc/bind/zones/$MYHOSTNAME.db << EOL
; BIND data file for $MYHOSTNAME
;
\$ORIGIN $MYHOSTNAME.
\$TTL 14400
@ IN SOA ns1.$MYHOSTNAME. host.$MYHOSTNAME. (
201006601 ; Serial
7200 ; Refresh
120 ; Retry
2419200 ; Expire
604800) ; Default TTL

$MYHOSTNAME. IN NS ns1.$MYHOSTNAME.
$MYHOSTNAME. IN NS ns2.$MYHOSTNAME.

$MYHOSTNAME. IN MX 10 mail.$MYHOSTNAME.
mail IN A $MAILSERVER_IP

$MYHOSTNAME. IN A WEBSERVER_IP

ns1 IN A 10.0.5.1
ns2 IN A 10.0.5.2
www IN CNAME $MYHOSTNAME.
EOL
#=================== reverse lookup ====================#
cat > /etc/bind/zones/rev.5.0.10.in-addr.arpa << EOL
\$ORIGIN 5.0.10.in-addr.arpa.
\$TTL 86400
@ IN SOA $MYHOSTNAME. host.$MYHOSTNAME. (
2010081401;
28800;
604800;
604800;
86400 );

@ IN NS ns1.$MYHOSTNAME.
@ IN NS ns2.$MYHOSTNAME.
80 IN PTR $MYHOSTNAME.
EOL
#===========================================================================#
```

### Webserver Setup
```
#!/bin/sh
#===========================================================================#
# install packages
apt-get install apache2
#===========================================================================#
echo "DONE"
#===========================================================================#
```

### FTP Server Setup and Config
```
#!/bin/sh
#===========================================================================#
# install packages
apt-get install vsftpd
apt-get install openssl
#===========================================================================#
# create certificate
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/vsftpd.pem -out /etc/ssl/private/vsftpd.pem
#===========================================================================#
# write configuration
> /etc/vsftpd.conf
cat >/etc/vsftpd.conf <<EOL
# Run standalone?  vsftpd can run either from an inetd or as a standalone
# daemon started from an initscript.
listen=NO
#
# This directive enables listening on IPv6 sockets. By default, listening
# on the IPv6 "any" address (::) will accept connections from both IPv6
# and IPv4 clients. It is not necessary to listen on *both* IPv4 and IPv6
# sockets. If you want that (perhaps because you want to listen on specific
# addresses) then you must run two copies of vsftpd with two configuration
# files.
listen_ipv6=YES
#
# Allow anonymous FTP? (Disabled by default).
anonymous_enable=NO
#
# Uncomment this to allow local users to log in.
local_enable=YES
#
# Uncomment this to enable any form of FTP write command.
write_enable=YES
#
# If enabled, vsftpd will display directory listings with the time
# in  your  local  time  zone.  The default is to display GMT. The
# times returned by the MDTM FTP command are also affected by this
# option.
use_localtime=YES
#
# Activate logging of uploads/downloads.
xferlog_enable=YES
#
# Make sure PORT transfer connections originate from port 20 (ftp-data).
connect_from_port_20=YES
#
# This option should be the name of a directory which is empty.  Also, the
# directory should not be writable by the ftp user. This directory is used
# as a secure chroot() jail at times vsftpd does not require filesystem
# access.
secure_chroot_dir=/var/run/vsftpd/empty
#
# This string is the name of the PAM service vsftpd will use.
pam_service_name=vsftpd
#
# This option specifies the location of the RSA certificate to use for SSL
# encrypted connections.
rsa_cert_file=/etc/ssl/certs/vsftpd.pem
rsa_private_key_file=/etc/ssl/private/vsftpd.pem
ssl_enable=YES
#
# More SSH config
allow_anon_ssl=NO
force_local_data_ssl=YES
force_local_logins_ssl=YES
ssl_tlsv1=YES
ssl_sslv2=NO
ssl_sslv3=NO
require_ssl_reuse=NO
ssl_ciphers=HIGH
#
# Uncomment this to indicate that vsftpd use a utf8 filesystem.
#utf8_filesystem=YES
#
# Add port ranges and allow local access
chroot_local_user=YES
pasv_min_port=40000
pasv_max_port=50000
#
# User Management
user_sub_token=$USER
local_root=/home/$USER/ftp
# Enable User Whitelist
#userlist_enable=YES
#userlist_file=/etc/vsftpd.userlist
#userlist_deny=NO
EOL
#===========================================================================#
# restart ftp server
service vsftpd restart
#===========================================================================#
echo "DONE"
#===========================================================================#
```

### Mail Server Setup
```
#!/bin/sh
#===========================================================================#
apt-get install apache2
apt-get install php
apt-get install postfix
service restart postfix
apt-get install dovecot-imapd
service restart dovecot
#===========================================================================#
echo "DONE"
#===========================================================================#
```

## IP Tables Script
```
#!/bin/bash

#===========================================================================#
#===================== DEBUGGING INFO ======================#

# Watch packets matching iptables rules
# watch -d iptables -L -v -n

# See open connections which pass the iptables rules => IP Tables State Top (top like)
# iptstate

# If ICP forward not working make sure that the following command returns 0!
# sysctl net.ipv4.icmp_echo_ignore_all

#===========================================================================#
#===========================================================================#

#========================= CONFIG ==========================#
# IP address + interface configuration
EXT_INTERFACE=ens33 # Interface for traffic out to internet
INTERNAL_SUBNET=10.0.5.0/24
GATEWAY_IP=192.168.10.135
EMAIL_HOST_IP=10.0.5.40
WEB_HOST_IP=10.0.5.80
#===========================================================================#
# Install basic necessary packages and enable forwarding
add-apt-repository universe
apt-get update
apt-get install -y iptables-persistent

sysctl -w net.ipv4.ip.forward=1 # Enable IP forwarding (try without -w if not working)
sysctl -p # List sysctl modifications
echo "net.ipv4.ip_forward = 1"  >> /etc/sysctl.conf # make change persistent

#===========================================================================#
# Empty all rules
## set default policies to let everything in
iptables --policy INPUT ACCEPT
iptables --policy OUTPUT ACCEPT
iptables --policy FORWARD ACCEPT
## start fresh
iptables -Z # zero counters
iptables -t nat -Z
iptables -F # flush (delete) rules
iptables -t nat -F
iptables -X # delete all extra chains

# Block everything by default
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

# Authorize already established connections
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#===========================================================================#
# NAT (Rewrite source IP of outgoing traffic to Gateway IP)
iptables -t nat -A POSTROUTING ! -d $INTERNAL_SUBNET -o $EXT_INTERFACE -j SNAT --to-source $GATEWAY_IP

#===========================================================================#
# ICMP (Ping)
iptables -A INPUT -p icmp -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p icmp -m state --state NEW -j ACCEPT
iptables -A FORWARD -p icmp -m state --state NEW -j ACCEPT

#===========================================================================#
# SSH
iptables -A INPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT

#===========================================================================#
# DNS
# Allow regular queries (input / output)
iptables -A OUTPUT -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p udp --dport 53 -m state --state NEW -j ACCEPT
iptables -A INPUT -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -A INPUT -p udp --dport 53 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p udp --dport 53 -m state --state NEW -j ACCEPT

#===========================================================================#
# HTTP + HTTPS
# Forward port 80 to Webserver
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 80 -j DNAT --to-destination $WEB_HOST_IP:80
iptables -A OUTPUT -p tcp --dport 80 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p tcp --dport 443 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 80 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 443 -m state --state NEW -j ACCEPT

#===========================================================================#
# Emails
# SMTP
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 25 -j DNAT --to-destination $EMAIL_HOST_IP:25
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 587 -j DNAT --to-destination $EMAIL_HOST_IP:587
iptables -A FORWARD -p tcp --dport 25 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 587 -m state --state NEW -j ACCEPT
# IMAP
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 143 -j DNAT --to-destination $EMAIL_HOST_IP:143
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 993 -j DNAT --to-destination $EMAIL_HOST_IP:993
iptables -A FORWARD -p tcp --dport 143 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 993 -m state --state NEW -j ACCEPT
# POP3
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 110 -j DNAT --to-destination $EMAIL_HOST_IP:110
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 995 -j DNAT --to-destination $EMAIL_HOST_IP:995
iptables -A FORWARD -p tcp --dport 110 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 995 -m state --state NEW -j ACCEPT

#===========================================================================#
# FTP
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 20:21 -j DNAT --to-destination $WEB_HOST_IP:20-21
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 40000:50000 -j DNAT --to-destination $WEB_HOST_IP:40000-50000
iptables -A FORWARD -p tcp --dport 20:21 -d $WEB_HOST_IP -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 40000:50000 -d $WEB_HOST_IP -m state --state NEW -j ACCEPT

#===========================================================================#
# Finalize
echo "DONE"
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTI5NDkxNzI2MiwtMjUwMzk4OTkxLC0xMz
gzMzE0NTU1LC0yMTE1MTE2MDAxLDEwNDE3NTkwMTZdfQ==
-->