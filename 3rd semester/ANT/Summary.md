
# Summary Advanced Networking VO
> by Florian Traxler

## Layers Recap

 1. Physical Layer
 2. Data Link Layer
 3. Network Layer
    - IP Addresses
    - ARP
    - ICMP
    - Routing
 4. Transport Layer 
    - TCP
    - UDP 
    - Ports
    - NAT
 5. (Session Layer)
 6. (Presentation Layer)
 7. Application Layer 

## Troubleshooting

### Failure/Error/Fault
|  |  |
|--|--|
|Failure|A component is not living up to its specifications  |
| Error | Part of a component that can lead to a failure |
| Fault | The cause of an Error |

#### Fault Types
- Transient Fault  
occur once and then disappear
- Intermittent fault  
occurs, vanishes, reoccurs - repeatedly
- Permanent fault  
is one that continues to exist until the faulty component is replaced

#### Fault Handling
- Prevention
- Fault tolerance (component can mask the occurred fault)
- Removal
- Forecasting (Estimate current presence, future incidence, and consequences of faults)

#### Failure types
- Crash: working correctly until it halts
- Omission Failure: fails to respond to requests
- Commission Failure: takes an action, that should not have been taken
- Timing Failure: response timed out
- Response Failure: incorrect response (value, sequence, ...)
- Arbitrary Failure: may produce arbitrary responses at arbitrary times

### Troubleshooting aproaches
- Top Down  
L7 -> L1  
if problem is on higher OSI Layer
- Bottom Up  
L1 -> L7  
if problem is on lower layer
- Divide and Conquer  
start at L3 and work in both directions  
very fast and effective approach
- Follow the Path  
follow the traffic and find the problem
- Spot the Differences  
compare configurations/setups  
might not reveal the real cause -> might be a workaround
- Move the Problem  
swap components and try to isolate the problem this way  
very expensive and time consuming
- Intuition

### Tools
The tool list is limited to *Linux*
- `arp`
- `ifconfig`
- `ip`
- `route`
- `ping`
- `traceroute`
- `netstat`
- `nslookup`
- `dig`
- `whois`
- `tcpdump`
- `tshark`
- `nmap`
- `telnet`
- `netcat`
- `hostname`

## NAT
*Network Address Translation*  
**Motivation**: Whole network has only one IP from the outside. The network management can be made independent from ISPs.

- internal addresses to one or more public addressses
- implemented in a router/gateway
- NAT/NAPT breaks the strict layering principle of the Internet as it operates on Layer 3/4
- Additional configuration may be required when incoming connections are required
- IPsec does not work with NAT
	- ESP tunnel mode: IP address is encapsulated in encrypted packet.
	- AH (tunnel and transport) mode: MAC (Message Authentication Code) based on the sender IP.
	- Special operation modes exist that enable IPsec NAT traversal.
- Security benefits of NAT:
	- Internal network structure is hidden to the public.
	- NAT often involves a firewall that rejects connections initiated from the public network.

### Implementation
| operation | what is done |
|--|--|
| outgoing datagrams: replace | replace `sourceIP:port` of every outgoing datagram to `natip:natPort` |
| remember | in NAT translation table store every `sourceIP:port` <-> `natIP:natPort` combination |
| incoming datagrams: replace | replace `natIP:natPort` in destination fields of incoming data with `sourceIP:port` found in NAT table |

### Network Address Port Translation
*also called NAPT, PAT, IP masquerading, NAT overload, many-to-one NAT, **NAT***  

### NAT Types
- Static NAT (one to one mapping)
	- private IP is mapped to a static public IP 
- Dynamic NAT (pool to pool mapping)
	- private IPis dynamically mapped to a public IP from an IP pool. (May differ over time)
- Dynamic NAT with PAT overloading
	- private IP is not always  the same `publicIP:port` combination
	- Implement with `MASQUERADE` when you have a dynamic public IP 
		- more CPU intensive
		- you need to specify an interface
	- Implement with `SNAT` when you have a static public IP
		- less CPU intensive
		- an IP address is specified

#### Port Forwarding
- Special destination NAT rule
- Forwards just one port

### UPnP
*Universal Plug and Play*  
- a host can discover and configure a nearby NAT
- enabling connections from outside possible -> security!
- no authentication

#### The Process
0. Addressing
1. Discovery  
	1. new device multicasts a number of discovery messages, advertising itself, its embedded devices, and its service.
	2. Any interested control point can listen to the standard multicast address for notifications that new capabilities are available.
	3. When a new control point is added to the network, it multicasts a discovery message searching for interesting devices or services.
	4. All devices must listen to the standard multicast address for these messages and must respond if any of their embedded devices or services match the search criteria in the discovery message.
2. Description  
The control point only knows information that was in the discovery message (the device's (or service's) UPnP type, the device's universally-unique identifier, and a URL to the device's UPnP description).  
The control point must retrieve a description (XML) of the device and its capabilities from the URL provided by the device in the discovery message.
3. Control  
Given knowledge of a device and its services, a control point can ask those services to invoke actions and receive responses indicating the result of the action
4. Eventing  
To subscribe to eventing, a subscriber sends a subscription message. If the subscription is accepted, the publisher responds with a duration for the subscription.
5. Presentation  
Presentation exposes an HTML-based user interface for controlling and/or viewing device status.

## Firewalls
Firewalls are a barrier between different network areas. The are used to enforce security policy by rules that authorize or deny traffic passing in each direction. While usually operating on Layer 3 or Layer 4,  some operate on higher Layers (Software Firewalls -> Deep Packet Inspection).

- They allow to protect the network against network-based security threats, while allowing connection to the wide area network.
- The firewall itself needs to be immune to penetration in terms of network-based threats or physical access.

### Inspection
- All traffic from the inside to the outside and vice versa needs to pass through the firewall.
- White-listing is applied to the traffic, only allowing authorized traffic.
- Traffic is filtered based on
	- service type
	- content type
	- the user accessing a resource
	- behaviour (packets per second)
- There is no protection against
	- internal attacks (users, malware from user devices)
	- insecure internal network structure (wireless)

### Architectures

#### Packet Filtering Firewall
Filtering is applied to all traffic. The rules can be based on: Source IP, Destination IP, Transport Layer Protocol, Transport Layer Address or Physical Interface.

|Advantages| Disadvantages |
|--|--|
| Very Simple | No possibility to track if a packet is an answer to another packet |
| Fast | No rules that determine the rate of packets transmitted within a connection |
|  | No option to track whether a connection is related to another (e.g. FTP) |
|  | Cannot prevent against application-specific vulnerabilities or specific functions |

##### Vulnerabilities
- IP address spoofing 
- Tiny Fragment Attack   
Extremely small fragments are created, which force the TCP header information to split up and distribute into separate packet fragments. Because Firewalls often only apply their ruleset to the first fragment, data can be pushed through the firewall without getting detected. Protection against this attack can be by rejecting fragmented packets or only allowing fragments with a certain amount of transport header information. 
- Overlapping Fragment Attack  
The offset of subsequent packet fragments is manipulated. When the packet is reassembled, header information from the first fragment might get overwritten. Defeated either by rejecting fragmented packets or assembling packets before filtering decision (slow).

#### Stateful Inspection Firewall
While Packet Filtering Firewalls make filtering decisions based on individual packets, Stateful Inspection Firewalls consider additional context.

##### Connection Handling
Via TCP, because Client server applications often rely on TCP as a transport layer protocol. “Well-known” port numbers (lower than 1,024) as destination ports and temporary ports (between 1,024 and 65,535) as source ports.  
Problems with connection handling in Packet Filtering Firewalls are that Incoming packets on all temporary ports need to be accepted in order not to block replies from the server side. This results in a vulnerability as it can also be exploited by an attacker.

##### Processing
- Outgoing packets are tracked as connections
- Incoming packets are allowed only if they fit to the outgoing packets in terms of source / destination address and port numbers

##### Tracked Information
- Same information as for packet filtering firewalls like source and destination address and port numbers.
- Some stateful firewalls also track TCP sequence numbers to prevent from attacks based on these.
- Limited amount of application layer data for wellknown protocols (e.g. FTP) to identify related connections.
- Connection entry is removed when a FIN/RST flag is noticed or a timeout occurs (UDP).

#### Higher Level Firewalls
- Deep Packet Inspection (checks the payload)
- Intrusion Detection System (monitors for malicious activities)
- Intrusion Prevention System (avoids malicious activities)
- NGFW (combination ofIntrusion Prevention and Deep Packet Inspection)

##### Implementation
High-availability cluster of two parallel hardware appliances needed.
- Active/Active or Active/Passive configuration with either load-balancing or transparent fail-over using virtual IP addresses.
- Instances monitor their status via a heartbeat connection.
- Status (e.g. connections) are synchronized between all instances.

### Firewall Placement

#### Integrations
- Stand-alone machine with general purpose hardware running a normal operating system
- Specialized hardware with a hardened small operating system
- Integrated as a software module in a router or switch

#### Placement
In general a firewall is placed as a protective barrier between external and internal network areas.
- Bastion Host: Critical strong point in the network
	- Secure operating system version results in a hardened system.
	- Reduced functionalities on the bastion host like proxies for DNS, FTP, HTTP, and SMTP.
	- Proxies operate as non-privileged user to increase security.
	- Can perform user authentication before access is granted.
	- Logs audit information for all traffic and each connection.
	- Filesystem of the bastion host can be read only to prevent injection of malicious code.
- Host-Based Firewall: Software module to secure an individual host
	- Available for many operating systems.
	- Often placed directly on the server which should be protected.
	- Filtering rules are tailored to the host environment.
	- Protection is independent from the network topology or location.
	- Also internal attacks (e.g. from vulnerable/compromised servers) are rejected.
- Personal Firewall: Software module to secure a personal computer
	- Less complex compared to network-wide firewalls.
	- Primary role is to deny unauthorized remote access to the computer.
	- Monitor outgoing activity to detect and block malware.
	- Can consider the originating application of traffic in order to allow or deny it.

##### Distributed Firewalls
To divide the network in different areas of security more than one firewall could be required.
- Distributed Firewalls are centrally managed and thus enforce a unique rule-set under one configuration interface.
	- Bastion, host and personal firewalls can be included in this system.
	- Log aggregation, analysis, statistics, and audit is performed on a central system.

##### DMZ
*De-Militarized-Zone*  
DMZ Network Separation of the network into different stages of security can look like this:
- Bastion host at the boundary router to protect the network against the WAN.
- Systems that are externally accessible but require protection (e.g. mail, DNS, web-server) are placed in a DMZ.
- External firewall enforces a basic set of security for all services and computers in the network.
- One or more internal firewalls protect the internal network at several stages and add more stringent filtering capabilities.
- Internal firewalls provide two-way protection between the internal protected network and the DMZ services.

### Iptables
As an exaple firewall, `iptables` from Linux is oberved.
![Netfilter Packet Traversal](iptables.png)


## ICMP, DHCP Recap

### ICMP
*Page 157-161*  
ICMP is a companion to IP that returns error info. Required, and used in many ways (e.g., for ping, traceroute)

### DHCP
*Page 163-167*  
DHCP assigns a local IP address to a host. Gets host started by automatically configuring it. The host sends a request to server, which grants a lease.
#### DHCP messages:
- host broadcasts `DHCP DISCOVER` message
- DHCP server responds with `DHCP OFFER` message
- host requests IP address: `DHCP REQUEST` message
- DHCP server sends address: `DHCP ACK` message

## Transport Layer

### Transport Layer Services
- provide logical communication between applications on different hosts
- sender breaks data into *segments*
- receiver side recreates *messages* out of the received segments
- TCP
	- reliable
	- in order delivery
	- congestion control
	- flow control 
	- connection setup
- UDP
	- unreliable 
	- unordered delivery
- Sockets
	- Messages are processed (sent/received)
	- TCP or UDP is choosen when a Socket is created

### Multiplexing - Demultiplexing

#### Multiplexing
Multiplexing at the sender side  
handle data from multiple sockets and add a transport header  

#### Demultiplexing
Demultiplexing on the receiver side  
Use header information to deliver received segments to their correct socket  
Segment contains source and destination port numbers  
Host uses IP addresses & port numbers to direct a segment to an appropriate socket

##### Connectionless demultiplexing
When the host receives an UDP segment, it checks destination port number of the received segment and directs it to the socket belonging to that port number. IP datagrams with the same destination port number, but with different source IP addresses and/or source port numbers will be directed to the same socket at the destination host.

##### Connection-oriented demultiplexing
Identified by source IP address, source port number, destination IP address and destination port number. All four values are used to determine the socket. Many simultaneous sockets may be supported (e.g. web server. Different sockets for each request)

#### Port Numbers
- System Ports (0-1023) (assigned by IETF)
- User Ports (1024-49151) (assigned by IANA)
- Dynamic and/or Private Ports (49152-65535) (not assigned)

### UDP
*connectionless transport*
- no connection establishment -> faster
- no congestion control-> faster
- simple
- smaller headers

#### Header
- source port
- destination port
- length
- checksum
	- covers UDP segment and IP pseudoheader
	- fields that change in the network are all set with `0`
	- end-to-end delivery check
- payload

### Principles of reliable data transfer
to ensure reliability even though packets may be lost, corrupted, delayed, and duplicated

#### Mechanisms
> Continuing on Slide 196
> Due to missing time. Please refer to Max's Summary, like I'll do ;-)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjA0OTY2ODQ0LDE4NzgwOTk4NzAsLTMyMT
k3MDE3NiwtMzM5MTIxMTU3LC00MTAxOTI4NjUsODc5ODAzMzQz
LC05NDM1NDI5NjAsMTk1MjExNjMxLDMxNDE2NjU1NywtMTU1Mj
U1NDgyOSwtNzIyMjcwNjY2LDE2Mjg0NTA4MjAsMTk3Mjg5MjM2
NCwyOTkwMzMyMTAsLTE4NjgzOTM3MjIsMTkzNjQzNzI4MV19
-->